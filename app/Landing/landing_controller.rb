require 'rho'
require 'rho/rhocontroller'
require 'rho/rhoerror'
require 'helpers/browser_helper'
require 'json'


class LandingController < Rho::RhoController
  include BrowserHelper   
  

  # GET /Landing
  def index
      @paramsfoojson = @params['fooJson']
        @partner = @redirectId
#        response =  Rho::AsyncHttp.get(:url => "http://app.qa.annectos.net/qa.mobile.annectos.api/tracker/m_profile?emp_id=#{$var}",
        response =  Rho::AsyncHttp.get(:url => "#{$varApi}login/m_profile/#{$var}",
        :headers => {"Content-Type" => "application/json"})
        @result = response["body"] 
        @pResult = Rho::JSON.parse(@result)
        puts @pResult
        @varFname = @pResult["user_f_name"]
        @varLname = @pResult["user_l_name"]
        @varEid = @pResult["user_email"]
        @varMob = @pResult["user_mob"]
        @varAdd = @pResult["user_address"]
#        puts @paramsfoojson
#        puts @paramsfoojson["user_f_name"]

    render :action => :index
  end
  
  
  def menu
    render :action => :menu
  end
  
#  def checkconn
#    Rho::Network.detectConnection( {:host => "#{$varDBHostName}", :port => 27017}, (url_for :action => :detect_connection_callback, :controller => :MainClassMethods))
#  end
#The above method is needed if connection status is connected and asyncronously reconnect
  
  def sign
#    Rho::Network.detectConnection( {:host => "#{$varApiHostName}", :port => 30010}, (url_for :action => :detect_connection_callback, :controller => :MainClassMethods))
    render :action => :sign
  end
  
  def activate
    
    @msg = @params['msg']
    render :action => :activate
  end
  
  def menudetails
    render :action => :menudetails
  end
  
  def navigatorIssue
    render :action => :navigatorIssue
  end
  
 
  def signup
      userfname = @params['user_f_name'].to_s
      userlname = @params['user_l_name'].to_s
      useremail = @params['user_email'].to_s
      userpassword = @params['user_password'].to_s
      usermob = @params['user_mob'].to_s
      useraddress = @params['user_address'].to_s
              tempHash = '{"user_f_name" : "'+userfname+'","user_l_name" : "'+userlname+'", "user_email" : "'+useremail+'", "user_password": "'+userpassword+'", "user_mob": "'+usermob+'", "user_address": "'+useraddress+'"}'             
              response =  Rho::AsyncHttp.post(:url => "#{$varApi}login/loginby",
                                              :headers => {"Content-Type" => "application/json"},
                                              :body => tempHash
                                              )
              @msg =  "Registration was Successful. Please sign in and activate your account"

            redirect :action => :activate, :query => {:msg => @msg}

            
     end

  
  def signinvalidate
    if @params['user_email'].to_s == ''
      @msg =  "Fields are mandatory"
      redirect :action => :activate, :query => {:msg => @msg}
    else
      email = @params['user_email'].to_s
      password = @params['user_password'].to_s
      tempHash = '{"user_email" : "'+email+'","user_password" : "'+password+'"}'     
      response =  Rho::AsyncHttp.post(:url => "#{$varApi}login/signin",
                                      :headers => {"Content-Type" => "application/json"},
                                      :body => tempHash
                                      )
      parsedR = Rho::JSON.parse(response["body"])
      $var = parsedR["user_email"]
        puts $var
      @f = parsedR["msg"]
        if @f == "Login Successful"
#          redirect :controller => :Landing, :action => :index, :query => {:fooJson => @variedResults}
          redirect :controller => :Landing, :action => :index
        else
            @msg =  "Details seems to be Incorrect"
            redirect :action => :activate, :query => {:msg => @msg}
      end
                                      
    end
  end

end
