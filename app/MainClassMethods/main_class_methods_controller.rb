require 'rho'
require 'rho/rhocontroller'
require 'helpers/browser_helper'

class MainClassMethodsController < Rho::RhoController
  include BrowserHelper

  # GET /MainClassMethods

  def detect_connection_callback
        if @params["connectionInformation"]=="Connected"
            # the server can be reached
          puts @params["connectionInformation"]
#          Rho::WebView.navigate(url_for(:action => :sign, :controller => :Landing))
        else
          puts @params["connectionInformation"]
          Rho::WebView.navigate(url_for(:action => :navigatorIssue, :controller => :Landing))
        end
    end

end
