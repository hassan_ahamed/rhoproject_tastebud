require 'rho/rhoapplication'
require 'helpers/browser_helper'


class AppApplication < Rho::RhoApplication

  def initialize
    $var ||= {}
    $varApi = "http://54.179.187.189:30010/"
    $varDBHostName = "http://54.179.187.189"
    $varApiHostName = "http://54.179.187.189"
    # Tab items are loaded left->right, @tabs[0] is leftmost tab in the tab-bar
    # Super must be called *after* settings @tabs!
    @tabs = nil
    #To remove default toolbar uncomment next line:
    @@toolbar = nil
    super


    # Uncomment to set sync notification callback to /app/Settings/sync_notify.
    # Rho::RhoConnectClient.setObjectNotification("/app/Settings/sync_notify")
    Rho::RhoConnectClient.setNotification('*', "/app/Settings/sync_notify", '')
    
  end
  
  
  
end
